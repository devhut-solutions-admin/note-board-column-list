import css from "./note-board-column-list.css";

var debug = false;
var debug1 = false;
var debugdragenter = false;
var debugdragleave = false;
var leavehome = false;
var debugautorearrange = false;
var debugdrop = false;
var debugmouseup = true;
var debugoldparent = false;
var debugdoshuffle = false;
var debugdatacrib = true;


/********************************
 * function doShuffle()
 * shuffle draggable items when
 * draggable item is dropped in dropzone
 * that already has a draggable item
 * in it
 * @prop originalOrder = dragstore.getOrderPlace() 
 * 		original position of dragged item
 * @prop newParent = dragstore.getNewParent()
 * 		new dropzone from which we can derive
 * @prop newOrder = newParent.dataset.dhDz
 * 		position of dropping
 * @prop shuffleUpOrDown 
 * 		if originalOrder < newOrder = "up"
 * 		if originalOrder > newOrder = "down"
 * @prop shuffleItem = dragstore.getItemToShuffle
 * 
 ********************************/
function doShuffle(){
	let originalOrderStr = dragstore.getOrderPlace();
	let originalOrder = parseInt(originalOrderStr, 10);
	let newParent = dragstore.getNewParent();
	let newOrderStr = dragstore.getNewParentOrder();
	let newOrder = parseInt(newOrderStr, 10);
	if(debugdoshuffle){
		console.log('neworder = ');
		console.log(newOrder);
	}
	let shuffleUpOrDown = '';
	if(originalOrder < newOrder){
		shuffleUpOrDown = 'up';
	}else{
		shuffleUpOrDown = 'down';
	}
	if(debugdoshuffle){
		console.log('shuffleUpOrDown = ' + shuffleUpOrDown);
	}
	if(shuffleUpOrDown === 'up'){
		let currShuffleToOrder = originalOrder;
		while(currShuffleToOrder < newOrder){
			let dz = getMatchingDropzone(currShuffleToOrder);
			let shuffleFromOrder = currShuffleToOrder + 1;
			let dragToShuffle = getDraggableToMove(shuffleFromOrder);
			let cloneofdrag = dragToShuffle.cloneNode(true);
			dragToShuffle.remove();
			// let documentFrag = new DocumentFragment();
			// documentFrag.append(dragToShuffle);
			dz.append(cloneofdrag);
			currShuffleToOrder = shuffleFromOrder;
		}
	}
	if(shuffleUpOrDown === 'down'){
		let currShuffleToOrder = originalOrder;
		while(currShuffleToOrder > newOrder){
			let dz = getMatchingDropzone(currShuffleToOrder);
			let shuffleFromOrder = currShuffleToOrder - 1;
			let dragToShuffle = getDraggableToMove(shuffleFromOrder);
			let cloneofdrag = dragToShuffle.cloneNode(true);
			dragToShuffle.remove();
			dz.append(cloneofdrag);
			currShuffleToOrder = shuffleFromOrder;
		}
	}
	dragstore.append();
	updateDraggableIds();
	dragstore.clearAllVars();
}

/******************************
 * function updateDraggleIds()
 * 	renumber draggableIds after
 * 	shuffling items for dnd
 ******************************/
function updateDraggableIds(){
	let dropzones = document.querySelectorAll('.dh-dropzone');
	for(var i = 0; i < dropzones.length; i++){
		let dropzone = dropzones[i];
		let children = dropzone.children;
		for(var j = 0; j < children.length; j++){
			if(children[j].classList.contains('dh-draggable')){
				let newid = i + 1;
				console.log('newid = ' + newid);
				children[j].id = newid;
			}
		}
	}
}



/*****************************
Closure-Based Drag and Drop
****************************/
let dragstore = (function () {
	let docFrag = new DocumentFragment();
	let dragged;
	let orderPlace;
	let oldParent;
	let newParent;
	let elPosEntered;
	let lastElPosEntered;
	let draggableToShuffle;
	let isShuffle = false;
	let shuffledEvents;
	return {
		getDragged: function () {
			return docFrag;
		},
		setDragged: function (el) {
			oldParent = el.parentElement;
			if(debugoldparent){
				console.log('oldParent = ' + oldParent);
			}
			docFrag.append(el);
			if (debug1) {
				console.log("setDragged = " + el);
				console.log("docfrag = ");
				console.log(docFrag);
			}
			dragged = el;
			
		},
		getOrderPlace: function () {
			return orderPlace;
		},
		setOrderPlace: function (order) {
			if (debug1) {
				console.log("setOrderPlace = " + order);
			}
			orderPlace = order;
		},
		remove: function () {
			dragged.remove();
		},
		append: function () {
			newParent.appendChild(docFrag);
			docFrag = new DocumentFragment();
		},
		replace: function () {
			oldParent.appendChild(docFrag);
			docFrag = new DocumentFragment();
		},
		hide: function () {
			dragged.classList.add("dh-item-hide");
		},
		show: function () {
			dragged.classList.remove("dh-item-hide");
		},
		hideParent: function () {
			oldParent.classList.add("dh-item-hide");
		},
		showParent: function () {
			oldParent.classList.remove("dh-item-hide");
		},
		setElPosEntered: function(pos){
			elPosEntered = pos
		},
		getElPosEntered: function(){
			return elPosEntered;
		},
		setLastElPosEntered: function(pos){
			lastElPosEntered = pos;
		},
		getLastElPosEntered: function(){
			return lastElPosEntered;
		},
		samePosElement: function(){
			if(elPosEntered === lastElPosEntered){
				return true;
			} else{
				return false;
			}
		},
		setNewParent: function(el){
			newParent = el;
		},
		getNewParent: function(){
			return newParent;
		},
		getNewParentOrder: function(){
			let dhorder = newParent.dataset.dhDz;
			return dhorder;
		},
		ShuffleEvent: function(){
			let dropzone = getDropzone(elPosEntered);
			let elementToMove = document
		},
		setDraggableToShuffle: function(el){
			draggableToShuffle = el;
			isShuffle = true;
		},
		getDraggableToShuffle: function(){
			return draggableToShuffle;
		},
		setIsShuffle: function(isShuf){
			isShuffle = isShuf;
		},
		getIsShuffle: function(){
			return isShuffle;
		},
		getOldParent: function(){
			return oldParent;
		},
		setOldParent: function(el){
			oldParent = el;
		},
		clearAllVars: function(){
			docFrag = new DocumentFragment();
			dragged = '';
			orderPlace = '';
			oldParent = '';
			newParent = '';
			elPosEntered = '';
			lastElPosEntered = '';
			draggableToShuffle = '';
			isShuffle = false;
		}

	};
})();

/*****************************
Closure-Based Drag and Drop
Original ( backup dragstore function)
****************************/
let Originaldragstore = (function () {
	let dragged;
	let orderPlace;
	let oldParent;
	let newParent;
	let elPosEntered;
	let lastElPosEntered;
	let shuffledEvents;
	return {
		getDragged: function () {
			return dragged;
		},
		setDragged: function (el) {
			if (debug1) {
				console.log("setDragged = " + el);
			}
			dragged = el;
			oldParent = dragged.parentElement;
		},
		getOrderPlace: function () {
			return orderPlace;
		},
		setOrderPlace: function (order) {
			if (debug1) {
				console.log("setOrderPlace = " + order);
			}
			orderPlace = order;
		},
		remove: function () {
			dragged.remove();
		},
		append: function (parent) {
			parent.appendChild(dragged);
		},
		replace: function () {
			oldParent.appendChild(dragged);
		},
		hide: function () {
			dragged.classList.add("dh-item-hide");
		},
		show: function () {
			dragged.classList.remove("dh-item-hide");
		},
		hideParent: function () {
			oldParent.classList.add("dh-item-hide");
		},
		showParent: function () {
			oldParent.classList.remove("dh-item-hide");
		},
		setElPosEntered: function(pos){
			elPosEntered = pos
		},
		getElPosEntered: function(){
			return elPosEntered;
		},
		setLastElPosEntered: function(pos){
			lastElPosEntered = pos;
		},
		getLastElPosEntered: function(){
			return lastElPosEntered;
		},
		samePosElement: function(){
			if(elPosEntered === lastElPosEntered){
				return true;
			} else{
				return false;
			}
		},
		setNewParent: function(el){
			newParent = el;
		},
		getNewParent: function(){
			return newParent;
		},
		ShuffleEvent: function(){
			let dropzone = getDropzone(elPosEntered);
			let elementToMove = document
		}

	};
})();
/****************************
Get Matching Dropzone
****************************/
function getMatchingDropzone(ind) {
	let i = parseInt(ind, 10);
	if(debugdoshuffle){
	console.log('i = ' + i);
	}
	// get all dropzones
	let dzs = document.querySelectorAll(".dh-dropzone");
	// loop all dropzones to find match and bump nodes up
	for (let j = 0; j <= dzs.length; j++) {
		// match dropzone to current orderPlace item
		// let dz = dzs[j];
		let dhorderStr = dzs[j].dataset.dhDz;
		let dhorder = parseInt(dhorderStr, 10);
		if(debugdoshuffle){
		console.log('dhorder = ' + dhorder);
		}
		if (dhorder === i) {
			if (debugdoshuffle) {
				console.log(`j=${j} i=${i} dhDz=${dhorder}`);
				console.log('returning dzs[j] = ');
				console.log(dzs[j]);
			}
			
			return dzs[j];
		}
	}
}
/******************************************
 * function getDraggableToMove
 ******************************************/
function getDraggableToMove(idstr){
	let id = parseInt(idstr, 10);
	let draggable = document.getElementById(id);
	// for(let i = 0; i < draggables.length; i++){
	// 	let draggable = draggables[i];
	// 	if(draggable.id === id){
	// 		if(debugdoshuffle){
	// 			console.log('getdraggabletomove = ');
	// 			console.log(draggable);
	// 		}
	// 		return draggable;
	// 	}
	// }
	if(debugdoshuffle){
		console.log('getdraggabletomove = ');
		console.log(draggable);
	}
	return draggable;
}
/****************************
getDropzone(num)
****************************/
function getDropzone(dorder) {
	let dropzones = document.querySelectorAll(".dh-dropzone");
	for (let i = 0; i < dropzones.length; i++) {
		let dropzone = dropzones[i];
		let data = dropzone.dataset;
		let dhdz = data.dhDz;
		if (dhdz === dorder) {
			return dropzone;
		}
	}
}

/************************************
 * AutoRearrange on DragEnter
 * 
 **************************************/
function autoRearrange(){
	let draggableOrder = dragstore.getOrderPlace();
	let posEntered = dragstore.getElPosEntered();
	/***********************************
	 * if moving element down
	 * 	draggableOrder < posEntered
	 * 	slide up items between draggableOrder
	 * 	and posEntered - including posEntered
	 **********************************/
	if(draggableOrder < posEntered){
		let dropzone = getDropzone(draggableOrder.toString());
	}
	if(debugautorearrange){
		console.log('draggableOrder = ', draggableOrder);
		console.log('posEntered = ', posEntered);
		console.log('dropzone = ', dropzone);
	}
}


/******************************
AddNewDropZone and Renumber Zones and Items
*******************************/
// function addNewDropzone(event) {
// 	if ("content" in document.createElement("template")) {
// 		var template = document.querySelector(".dh-template-drag-placeholder");
// 		var clone = template.content.cloneNode(true);
// 		event.target.appendChild(clone);
// 	}
// }

/***************************
Cursor coordinate Tracking
*************************/
let tracking = (function trackCursor() {
	let cursorX;
	let cursorY;
	function updateCursor(event) {
		cursorX = event.clientX;
		cursorY = event.clientY;
		if (debug1) {
			console.log(cursorX);
			console.log(cursorY);
		}
	}
	return {
		start: function () {
			document.addEventListener("mousemove", updateCursor);
		},
		stop: function () {
			document.removeEventListener("mousemove", updateCursor);
		}
	};
})();

// var dragged;

/* events fired on the draggable target */
document.addEventListener("drag", function (event) {}, false);

document.addEventListener(
	"dragstart",
	function (event) {
		console.log('dragstart');
		
		// // store a ref. on the dragged elem
		// dragged = event.target;
		// // make it half transparent
		// event.target.style.opacity = 1.0;
		event.effectAllowed = "move";
		event.dataTransfer.setData("text/html", event.target);
		/***************************
		Make use Of DragStore IIFE
		***************************/
		dragstore.setDragged(event.target);
		dragstore.setOrderPlace(event.target.id);
		let follower = setCustomDragImage();
		/*************************
		Register event Listener for mouseup
		***************************/
		document.addEventListener("mouseup", function(event){
			follower.remove();
			document.body.classList.remove('dh-cursor-hide');
			let ismove = false;
			let cursorX = event.clientX;
			let cursorY = event.clientY;
			if(document.elementsFromPoint){				
				let elems = document.elementsFromPoint(cursorX, cursorY);
				elems.forEach(function(val,ind,arr){
					let elem = elems[ind];
					/********************************
					 * if mouseup over a dropzone
					 * append dragged item to dropzone
					 * and shuffle current items 
					 *********************************/
					if(elem.classList.contains('dh-dropzone')){
						ismove = true;
						dragstore.setNewParent(elem);
						if(debugmouseup){
						console.log('newparent.dataset.dh-dz = ');
						console.log(dragstore.getNewParent());
						}
					}									
				})
			}
			if(ismove === true){
				/******************************
				 * if there is a shuffle to be done
				 * then call the doShuffle() method
				 ******************************/
					doShuffle();
				
			}else{
				if(debugmouseup){
					console.log('ismove = ' + ismove);
				}
				dragstore.replace();
			}
		})
	},
	false
);

// document.addEventListener(
// 	"dragend",
// 	function (event) {
// 		console.log('dragend');
// 		// reset the transparency
// 		event.target.style.opacity = "";
// 		tracking.stop();
// 	},
// 	false
// );

// /* events fired on the drop targets */
// document.addEventListener(
// 	"dragover",
// 	function (event) {
// 		// prevent default to allow drop
// 		if (event.target.className === "dh-dropzone") {
// 			event.preventDefault();
// 		}
// 	},
// 	false
// );

// document.addEventListener(
// 	"dragenter",
// 	function (event) {				
// 		if (event.target.className === "dh-dropzone") {
// 			event.preventDefault();
// 			/**
// 			 * get order of element being dragged
// 			 */
// 			let draggedElPosition = dragstore.getOrderPlace();
// 			/**
// 			 *  get element being entered
// 			 */
// 			let dragEnterEl = event.originalTarget;
// 			/**
// 			 *  get dataset of element being entered
// 			 */
// 			let dragEnterElDataset = dragEnterEl.dataset;
// 			/**
// 			 *  get value of data-dh-dz of dataset of element entered
// 			 */
// 			let dragEnterPosition = dragEnterElDataset.dhDz;
// 			/**
// 			 *  store position number of element just entered
// 			 */
// 			dragstore.setElPosEntered(dragEnterPosition);
// 			/**
// 			 *  make sure dragenter is not firing on dragged
// 			 *  element's original container
// 			 */
// 			if(draggedElPosition !== dragEnterPosition){
// 				/**
// 				 *  check to see if dragenter is firing a second
// 				 *  time on the same container element
// 				 */
// 				let checkIfSame = dragstore.samePosElement();
// 				if(!checkIfSame){
// 					event.target.style.background = "#00ffff";
// 					autoRearrange();


// 				}
// 				if(debugdragenter){
// 					console.log('dragenterPosition = ' + dragEnterPosition)
// 					console.log('checkIfSame = ' + checkIfSame);
// 				}
				
				
// 				if(debugdragenter){
// 					console.log(event);
// 				}
// 			}
// 		}
// 	},
// 	false
// );

// document.addEventListener(
// 	"dragleave",
// 	function (event) {
// 		// reset background of potential drop target when the draggable element leaves it
// 		if (event.target.className == "dh-dropzone") {
// 			event.target.style.background = "";
// 			let dhDz = event.target.dataset.dhDz;
// 			if(debugdragleave){console.log(`dragleave elOrder = ${dhDz}`)}
// 			dragstore.setLastElPosEntered(dhDz);
// 		}
// 	},
// 	false
// );

// document.addEventListener(
// 	"drop",
// 	function (event) {
// 		console.log('drop');
// 		// prevent default action (open as link for some elements)
// 		event.preventDefault();
// 		if (debug) {
// 			console.log(event.target);
// 		}
// 		// move dragged elem to the selected drop target
// 		if (event.target.className == "dh-dropzone") {
// 			event.target.style.background = "";
// 			// dragged.parentNode.removeChild(dragged);
// 			if(debugdrop){
// 			console.log(dragstore.getDragged());
// 			}
// 			dragstore.append(event.target);
// 		}else{
// 			// no dropzone target so putback to original place
// 			let parent = dragstore.oldParent();
// 			if(debugdrop){
// 				console.log('parent = ', parent);
// 			}
// 		}

// 	},
// 	false
// );

function getCustomDragImg(event) {
	const canvas = document.createElement("canvas");
	canvas.width = 400;
	canvas.height = 100;

	const ctx = canvas.getContext("2d");
}

/**************************************
 * utility functions for handling task list
 * and order of items
 * 



/***
 * @Description superclass for dh-trello-<objects>
 * @Property  id : new uuid()
 * @Property  index : ''
 * @Property  parent : ''
 * @Property  children : []
 * @Property  state : 'none'
 * @Property  type : 'Abstract'
 * @Property  prototype : 'Object'
 * @Property
 */
function Abstract() {
	// this.uuid = function () {
	// 	return Math.random().toString(16).slice(2);
	// };
	this.name = '';
	this.id = this.uuid();
	this.index = '';
	this.parent = '';
	this.children = [];
	this.state = 'none';
	this.type = 'Abstract';
	this.prototype = 'Object';
	this.hookCreated();
}

Abstract.prototype = Object.create(Object.prototype);
Abstract.prototype.constructor = Abstract;
Object.defineProperties(Abstract, {
	name: {
		value: 'abby',
		writable: true,
		enumerable: true,
		configurable: true,
	},
});
Abstract.prototype.uuid = function(){
	return Math.random().toString(16).slice(2);
}

/***
 * @Methods
 */
/***
 * @setters
 */
Abstract.prototype.setName = function (name) {
	this.name = name;
};
Abstract.prototype.setDescription = function (description) {
	this.description = description;
};
Abstract.prototype.setIndex = function (ind) {
	this.index = ind;
};
Abstract.prototype.setType = function (type) {
	this.type = type;
};
Abstract.prototype.setParent = function (parent) {
	this.parent = parent;
};
/***
 * @TODO recursively adjust children types when
 *				re-typing an Object
 */
Abstract.prototype.addChild = function (childId) {
	this.children.push(childId);
};

/***
 * @getters
 */
Abstract.prototype.getId = function () {
	return this.id;
};
Abstract.prototype.getName = function () {
	return this.name;
};
Abstract.prototype.getIndex = function () {
	return this.index;
};
Abstract.prototype.getDescription = function () {
	return this.description;
};
Abstract.prototype.getType = function () {
	return this.type;
};
Abstract.prototype.getParent = function () {
	return this.parent;
};
Abstract.prototype.getChildren = function () {
	const iterator = this.children.entries();
	return iterator;
};
Abstract.prototype.findChild = function (id) {
	function matchID(el, ind, arr) {
		if (el.id === id) {
			return true;
		} else {
			return false;
		}
	}
	let matches = this.children.filter(matchId);
	if (matches) {
		return matches[0];
	} else {
		return 'none';
	}
};

/***
 *		@Events
 */
Abstract.prototype.setState = function (state) {
	this.state = state;
};
Abstract.prototype.getState = function () {
	return this.state;
};
Abstract.prototype.setLifecycle = function (lifecycle) {
	this.lifecycle = lifecycle;
};
Abstract.prototype.activateHook = function (hook) {
	const hookfunc = `hook${hook}`;
	const result = new Promise(this.hookfunc());
};

/***
 * hooks
 */

Abstract.prototype.hookCreated = function () {
	this.state = 'created';
};

Abstract.prototype.slotChange = function () {
	this.state = 'slotChangePending';
};

Abstract.prototype.hookArchive = function (id) {
	this.state = 'archivePending';
	this.insertBefore = id;
};

Abstract.prototype.hookMove = function () {
	this.state = 'movePending';
};

Abstract.prototype.hookChange = function () {
	this.state = 'changePending';
};

/***
 * @Desc logs all props and values to console
 */
Abstract.prototype.log = function () {
	console.table([
		{ var: 'id', value: this.id },
		{ var: 'name', value: this.name },
		{ var: 'index', value: this.index },
		{ var: 'type', value: this.type },
		{ var: 'prototype', value: this.prototype },
		{ var: 'parent', value: this.parent },
		{ var: 'children', value: this.children },
		{ var: 'state', value: this.state },
	]);
};

/***
 * @exports Abstract() as default
 */
// export { Abstract as default };
// export { Abstract };

/***
 * @Description Task (board) inherits from Abstract
 */
function Task() {
	Abstract.call(this);
	this.type = 'Task';
	this.description = 'Default description';
	this.prototype = 'Abstract';
}

Task.prototype = Object.create(Abstract.prototype);

Task.prototype.constructor = Task;

// export { Task as default };
// export { Task };
/***
 * @Description List (board) inherits from Abstract
 */
function List() {
	Abstract.call(this);
	this.type = 'List';
}

List.prototype = Object.create(Abstract.prototype);

List.prototype.constructor = List;

// export { List as default };
// export { List };

/***********************************
 * Create a List() object
 * Create 4 Task() objects
 * Set Tasks as children of List
 * 
 ************************************/
let datacrib = (function(){
	let collections = [];
	let lists = [];
	let tasks = [];
	return {
		setCollection: function(collection){
			collections.push(collection);
		},
		getCollectionsIterator: function(){
			const iterator = collections.values();
			return iterator;
		},
		setList: function(list){
			lists.push(list);
		},
		getListsIterator: function(){
			const iterator = lists.values();
			return iterator;
		},
		setTask: function(task){
			tasks.push(task);
		},
		getTasksIterator: function(){
			const iterator = tasks.values();
			return iterator;
		}
	}
})();



var newtasks = ['task one', 'task two', 'task three', 'task four'];
var dhSampleList = new List();
dhSampleList.setName('dhSampleList');



function createTask(name, index){
	let task = new Task();
	task.setName(name);
	task.setIndex(index);
	return task;
}

newtasks.forEach(function(val,ind,arr){
	let task = createTask(val, ind);
	dhSampleList.addChild(task.id);
	datacrib.setTask(task);
});

datacrib.setList(dhSampleList);
if(debugdatacrib){
	let taskIter = datacrib.getTasksIterator();
	for(const task of taskIter){
		console.log(task);
	}
}

/***************************************
 * function setCustomDragImage()
 * 
 ***************************************/
function setCustomDragImage(){
	document.body.classList.add('dh-cursor-hide');
	var follower;
	var init; 
	var mouseX; 
	var mouseY; 
	var positionElement;
	var printout;
	
	var template = document.querySelector('.dh-custom-img-template');
	var clone = template.content.cloneNode(true);
	var cloneParent = document.querySelector('.dh-document');
	cloneParent.append(clone);
	follower = document.querySelector('.dh-custom-img');
	  
	// printout = document.getElementById('printout');
	  
	mouseX = (event) => {
		return event.clientX;
	};
	
	mouseY = (event) => {
		return event.clientY;
	};
	
	positionElement = (event) => {
		var mouse;
		mouse = {
		x: mouseX(event),
		y: mouseY(event)
		};
		follower.style.top = mouse.y + 'px';
		return follower.style.left = mouse.x + 'px';
	};
	
	  
	window.onmousemove = init = (event) => {
		var _event;
		_event = event;
		return timer = setTimeout(() => {
		return positionElement(_event);
		}, 1);
	};
	
	return follower;
}
/*
export { debug, debug1, debugdragenter, debugdragleave, leavehome, debugautorearrange, debugdrop, debugmouseup, debugoldparent, debugdoshuffle, debugdatacrib, doShuffle, updateDraggableIds, dragstore, getMatchingDropzone, getDraggableToMove, getDropzone, autoRearrange, getCustomDragImg, Abstract, Task, List, datacrib, newtasks, dhSampleList, createTask, setCustomDragImage}
*/